[![pipeline status](https://gitlab.com/rsihammou/aws-k8s-app/badges/master/pipeline.svg)](https://gitlab.com/rsihammou/aws-k8s-app/commits/master)

# Aperçu

Dans ce projet nous allons construire une simple application (CRUD) php utilisant une base de donnée mysql.  
À l'aide de Terraform, et en se servant de provider kubernetes nous arrivons à déployer notre application 
sur le cluster k8s préalablement crée [aws-k8s-infra](https://gitlab.com/rsihammou/aws-k8s-infra).  
Le processus de déploiement est automatisé grâce au gitlab-ci.

# Schéma d'architecture

![k8s-app-architecture](images/k8s-app-archi.png)

À chaque commit le build de l'application est lancé automatiquement, les images sont construites 
et pusher vers le gitlab container registry.  
Terraform encapsule les manifestes kubernetes dans le but de faciliter la gestion de cycle de vie des déploiements, 
ceci est possible via les states et les plans d'exécution propre à Terraform.  
Le déploiement est opéré sur le cluster kubernetes installé sur le cloud privé AWS. Une fois le certificat 
TLS est propagée, l'application devient accessible en https en passant par le nom de domaine pré-configuré.

# Application web

Notre application web est une simple réalisation CRUD pour gérer les utilisateurs, 
elle est packagée sous forme de deux images Docker.

- **Image php-web :**

```dockerfile
FROM php:7.1-apache
# Install the PHP dependencies
RUN set -ex; \
   apt-get update; \
   docker-php-ext-install -j$(nproc) pdo_mysql; \
   rm -rf /var/lib/apt/lists/*
COPY ./src/ /var/www/html 
RUN a2enmod rewrite
```

- **Image mysql :**

```dockerfile
FROM mysql:5.7
ADD ./initdb.sql /docker-entrypoint-initdb.d
```

# Manifestes kubernetes

## Déploiements et services

Pour déployer l’application, nous avons configuré les ressources K8s suivantes :

- **deployment-mysql :** spécifie comment déployer une base de donnée mysql avec un volume persistent de type `hostpath`.
- **service-mysql :** fournit un point d’accès à la base de donnée, interne au cluster.
- **deployment-web :** spécifie comment déployer l'application web, deux instances seront déployées (`replicas= 2`). 
- **service-web :** fournit un point d'accès interne à l'application.

Comme indiqué ci-dessous, les services ont une adresse IP privée au cluster et ne possèdent pas d’adresse IP externe (`EXTERNAL-IP`).

![kubectl-service](images/kubectl-service.png)

Les services ne sont pas exposés sur Internet, ils sont accessibles en interne via leur nom.

##  Exposition du Service web

Pour rendre le service web accessible à l'extérieur du cluster, nous configurons une ressource Ingress. 
Nous utilisons l’implémentation ingress-nginx basée sur Nginx.  
La ressource Ingress doit spécifier :

- Le nom de domaine de l’application.
- Les règles de redirection des requêtes HTTP reçues vers le Service K8s. 
- L’implémentation Ingress à utiliser, pour notre cas implémentation nginx.

```hcl
resource "kubernetes_ingress" "web_ingress" {
  metadata {
    name = "web-ingress"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx" (implémentation ingress-nginx)
    }
  }
  spec {
    rule {
      host = var.common_name  //Nom de domaine de l'application
      http {
        path {
          backend {
            service_name = element(kubernetes_service.svc_web.metadata.*.name, 0) (nom du Service)
            service_port = kubernetes_service.svc_web.spec[0].port[0].port
          }
          path = "/"
        }
      }
    }
  }
}
```

Au déploiement de l’Ingress, la configuration du serveur Nginx est modifiée pour intégrer les règles de redirection HTTP.  
Ainsi, toutes les requêtes envoyées au host référencé par la variable `common_name` sont redirigées vers le service web (`svc-web`).

##  Création du Certificat TLS

### Cert-manager

Nous utiliserons le module `cert-manager` couplé à l’autorité de certification `Let’s Encrypt` pour 
créer le certificat TLS de notre projet.  
Let's Encrypt utilise le protocole ACME (Automatic Certificate Management Environment) pour vérifier 
que vous êtes le propriétaire du nom de domaine lié à la demande de certificat.  
Le client doit relever un challenge. Il existe différents types (`HTTP-01`, `DNS-01`, …) qui ont 
des contraintes différentes.  
Dans notre cas, nous avons choisi d’utiliser le challenge `DNS-01`. Dans ce challenge, le client 
crée un enregistrement TXT de nom `_acme-challenge.<YOUR_DOMAIN>` valorisé avec un token fourni 
par Let’s Encrypt.

Voici le processus de création de certificat :

![cert-manager](images/cert-manager.png)

1. Initialiser la demande de certificat via la création d'une ressource Certificate.
2. Cert-Manager récupère les informations du Certificat. 
3. Cert-Manager récupère les informations de configuration de l’Issuer indiqué dans le Certificat.
4. Cert-Manager va créer un challenge, ainsi que le nécessaire pour résoudre le challenge.
5. Cert-Manager demande un certificat au serveur configuré dans l’Issuer (staging ou production).
6. Let's Encrypt vérifiera la disponibilité et la validité de challenge et créera le certificat
7. Cert-Manager récupère le certificat et le stocke dans un Secret Kubernetes.

### ClusterIssuer

Pour que cert-manager puisse demander la création de certificats, il faut configurer un Issuer (ou un ClusterIssuer).
La différence entre un issuer et un clusterIssuer est que ce dernier est visible au niveau du cluster et 
pas seulement dans un namespace.

[clusterissuer.tf](clusterissuer.tf)

```hcl
resource "kubernetes_manifest" "cluster_issuer" {
  provider = kubernetes-alpha
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind = "ClusterIssuer"
    metadata = {
      name = var.cluster_issuer_name
    }
    spec = {
      acme = {
        email = var.acme_registration_email
        server = var.acme_staging_server_url
        privateKeySecretRef = {
          name = "lets-encrypt"
        }
        solvers = [{
          dns01 = {
            route53 = {
              region = var.aws_region
              hostedZoneID = data.aws_route53_zone.selected.zone_id
            }
          }
        }]
      }
    }
  }
}
```

### Ressource Certificat 

Pour générer le certificat, il faut d’abord créer une ressource `Certificate`.  
Le `Certificate` représente une demande de création du certificat envoyé à l’Issuer. 
Si les challenges passent, cette demande se conclue par la création d’un certificat stocké dans 
un secret kubernetes référencé par la variable `acme_secret_name`.  
Le `Certificat` contient notamment les informations suivantes : 

1. Le nom DNS pour lequel on souhaite un certificat. 
2. Le type et le nom de l’Issuer à utiliser. 
3. Le nom du Secret qui contiendra le certificat. 

[acme-certificate.tf](acme-certificate.tf)

```hcl
resource "kubernetes_manifest" "acme_cert" {
  provider = kubernetes-alpha
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind = "Certificate"
    metadata = {
      name = "acme-cert"
      namespace = "cert-manager"
    }
    spec = {
      secretName = var.acme_secret_name
      dnsNames = [var.common_name]
      issuerRef = {
        kind = kubernetes_manifest.cluster_issuer.manifest.kind
        name = element(kubernetes_manifest.cluster_issuer.manifest.metadata.*.name, 0)
      }
      commonName = var.common_name
    }
  }
}
```

### Configuration Ingress

Au niveau de la configuration de l’Ingress, il faut spécifier :

1. La liste des hosts à exposer en HTTPS 
2. Le nom du Issuer à utiliser 
3. Le nom du secret contenant le certificat SSL à utiliser.

[ingress.tf](ingress.tf)

```hcl
resource "kubernetes_ingress" "web_ingress" {
  metadata {
    name = "web-ingress"
    annotations = {
      "cert-manager.io/cluster-issuer" = element(kubernetes_manifest.cluster_issuer.manifest.metadata.*.name, 0) //nom du Issuer
      "kubernetes.io/ingress.class" = "nginx"
      "cert-manager.io/acme-challenge-type" = "dns01"
    }
  }
  spec {
    tls {
      hosts = [var.common_name] //hosts à exposer en HTTPS
      secret_name = var.acme_secret_name  //secret contenant le certificat SSL
    }
    rule {
      host = var.common_name
      http {
        path {
          backend {
            service_name = element(kubernetes_service.svc_web.metadata.*.name, 0)
            service_port = kubernetes_service.svc_web.spec[0].port[0].port
          }
          path = "/"
        }
      }
    }
  }
}
```

# GitLab CI-CD

Le fichier `.gitlab-ci.yml` contient la configuration de processus d'intégration et déploiement continue.   
Le processus est composé de la liste des jobs suivantes :

![pipeline-gitlab-ci](images/pipeline-gitlab-ci.png)

- **build_docker_images :** permet de builder les images docker (php-web et mysql) et les pusher vers le container registry.
- **validate_terrafom :** permet de valider le projet Terraform et détecter des éventuelles erreurs de syntaxe 
- **deploy :** lance le déploiement de l'application dans le cluster kubernetes, ce job dépend de deux précédents jobs.
- **cleanup_apply :** activé dans le cas ou le job deploy est en échec, il permet de nettoyer les éléments crées par le déploiement  
- **destroy :** détruit le déploiement effectué au préalable, ce job ne peux pas s'exécuter qu'après le deploy job.

## Prérequis

Avant de pouvoir démarrer le pipeline, il faut effectuer quelques réglages :

#### Configurer le backend terraform :

Utilisez le service AWS S3 pour créer un compartiment S3.

![s3-bucket](images/s3-bucket.png)

Rapporter la configuration de compartiment S3 dans le fichier [terraform.tf](terraform.tf)

```hcl
terraform {
  backend "s3" {
    bucket = "fr.alltech.terraform"
    key    = "kubernetes/state"
    region = "eu-west-3"
  }
}
```

#### Ajouter les variables gitLab-ci :

Rendez-vous sur `settings -> CI-CD ->variables`, et créez les variables suivantes :

![variables-gitlab-ci](images/variables-gitlab-ci.png)

- **`AWS_CONF` :** variable de type file contenant le contenu en base 64 de votre configuration AWS. 
Il s'agit du contenu du fichier `~/.aws/credentials` si vous avez déjà configuré `aws-cli` 
dans votre environnement du travail.  
Le contenu du fichier ressemble à :  
```ini
[default]
aws_access_key_id = ##########
aws_secret_access_key = #############
```

- **`K8S_CONF` :** variable de type file contenant le contenu en base 64 du fichier de configuration de 
votre cluster k8s (`~/.kube/config`)

- **`MYSQL_PASSWORD` :** mysql password en base 64.

# Accès à l'application web

Avant de déployer l'application, il faut s'assurer que votre domaine est bien configurer.  
Dans notre cas nous utilisons un domaine gratuit valable 3 mois à créer sur le site c

Vous devez récupérer les names servers de la zone hébergée route 53 en sortie de job `apply` 
du projet [aws-k8s-infra](https://gitlab.com/rsihammou/aws-k8s-infra).

![names-servers](images/names-servers.png)

Ensuite, il faut mettre à jour le domaine, toujours sur le site [www.freenom.com](www.freenom.com)

Onglet : `services ->My Domains ->Manage Domain ->Manage Free DNS -> Edit Nameservers`

![edit-nameservers](images/edit-nameservers.png)

Une fois le déploiement s'est bien déroulé, connectez-vous à votre master k8s et vérifiez que le certificat 
TLS a été bien propagée.

```sh
kubectl get certs,certificaterequests,order,challenges,ingress -o wide 
```
![verify-certificate](images/verify-certificate.png)

Dans le cas favorable vous pouvez accéder à l'application via l'url : [https://votre-domaine](https://votre-domaine).

![app-web](images/app-web.png)



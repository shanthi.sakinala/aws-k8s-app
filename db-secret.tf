resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }
  data = {
    db-password = var.mysql_password
  }
  type = "Opaque"
}
FROM php:7.1-apache

# Install the PHP dependencies
RUN set -ex; \
	apt-get update; \
	docker-php-ext-install -j$(nproc) pdo_mysql; \
	rm -rf /var/lib/apt/lists/*

COPY ./src/ /var/www/html 
RUN a2enmod rewrite


resource "kubernetes_persistent_volume" "pv_mysql" {
  metadata {
    name = "pv-mysql"
  }
  spec {
    capacity = {
      storage = "1Gi"
    }
    access_modes = ["ReadWriteMany"]
    persistent_volume_source {
      host_path {
        path = var.mysql_dir
      }
    }
  }
}


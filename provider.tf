# Provider
provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "kubernetes-alpha" {
  config_path = "~/.kube/config"
}

provider "aws" {
  version    = "~> 2.0"
  region     = "eu-west-3"
}

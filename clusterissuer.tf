resource "kubernetes_manifest" "cluster_issuer" {
  provider = kubernetes-alpha
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind = "ClusterIssuer"
    metadata = {
      name = var.cluster_issuer_name
    }
    spec = {
      acme = {
        email = var.acme_registration_email
        server = var.acme_prod_server_url
        privateKeySecretRef = {
          name = "lets-encrypt"
        }
        solvers = [{
          dns01 = {
            route53 = {
              /*accessKeyID = var.aws_access_key_id
              secretAccessKeySecretRef = {
                name = element(kubernetes_secret.route53-secret.metadata.*.name, 0)
                key = "secret-access-key"
              }*/
              region = var.aws_region
              hostedZoneID = data.aws_route53_zone.selected.zone_id
            }
          }
        }]
      }
    }
  }
}
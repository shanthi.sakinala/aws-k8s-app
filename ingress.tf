resource "kubernetes_ingress" "web_ingress" {
  metadata {
    name = "web-ingress"
    annotations = {
      "cert-manager.io/cluster-issuer" = element(kubernetes_manifest.cluster_issuer.manifest.metadata.*.name, 0)
      "kubernetes.io/ingress.class" = "nginx"
      "cert-manager.io/acme-challenge-type" = "dns01"
    }
  }
  spec {
    tls {
      hosts = [var.common_name]
      secret_name = var.acme_secret_name
    }
    rule {
      host = var.common_name
      http {
        path {
          backend {
            service_name = element(kubernetes_service.svc_web.metadata.*.name, 0)
            service_port = kubernetes_service.svc_web.spec[0].port[0].port
          }
          path = "/"
        }
      }
    }
  }
}
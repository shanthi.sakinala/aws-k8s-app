resource "kubernetes_manifest" "acme_cert" {
  provider = kubernetes-alpha
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind = "Certificate"
    metadata = {
      name = "acme-cert"
      namespace = "cert-manager"
    }
    spec = {
      secretName = var.acme_secret_name
      dnsNames = [var.common_name]
      issuerRef = {
        kind = kubernetes_manifest.cluster_issuer.manifest.kind
        name = element(kubernetes_manifest.cluster_issuer.manifest.metadata.*.name, 0)
      }
      commonName = var.common_name
    }
  }
}

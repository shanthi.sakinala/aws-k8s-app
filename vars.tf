variable "aws_region" {
  description = "The aws region"
}

variable "mysql_mount_path" {
  description = "The sql mount path for persistent storage"
}
variable "mysql_password" {

}
variable "mysql_img" {

}
variable "mysql_dir" {

}
variable "mysql_lbl" {

}

variable "db_name" {

}

variable "web_port" {
  description = "The web http port"
  default = "80"
}
variable "web_img" {

}

variable "common_name" {

}
variable "web_dir" {

}
variable "web_lbl" {

}
variable "cluster_issuer_name" {
  default = "letsencrypt-issuer"
}

variable "acme_secret_name" {
  default = "acme-tls-secret"
}

variable "acme_staging_server_url" {
  default = "https://acme-staging-v02.api.letsencrypt.org/directory"
}

variable "acme_prod_server_url" {
  default = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "acme_registration_email" {

}



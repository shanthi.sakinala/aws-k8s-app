resource "kubernetes_deployment" "dep_mysql" {
  metadata {
    name = "deploy-mysql"
    labels = {
      app = var.mysql_lbl
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.mysql_lbl
      }
    }
    template {
      metadata {
        labels = {
          app = var.mysql_lbl
        }
      }
      spec {
        container {
          image = var.mysql_img
          name  = var.mysql_lbl
          //image_pull_policy = "IfNotPresent"
          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = element(kubernetes_secret.mysql.metadata.*.name, 0)
                key = "db-password"
              }
            }
          }
          env {
            name = "MYSQL_DATABASE"
            value = var.db_name
          }
          port {
            container_port = 3306
            name = var.mysql_lbl
          }
          volume_mount {
            name = "mysql-persistent-storage"
            mount_path = var.mysql_mount_path
          }
        }
        volume {
          name = "mysql-persistent-storage"
          persistent_volume_claim {
            claim_name = element(kubernetes_persistent_volume_claim.pvc_mysql.metadata.*.name, 0)
          }
        }
      }
    }
  }
}

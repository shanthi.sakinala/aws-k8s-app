resource "kubernetes_service" "svc-mysql" {
  metadata {
    name = "svc-mysql"
  }
  spec {
    selector = {
      app = var.mysql_lbl
    }
    port {
      port = 3306
    }
    cluster_ip = "None"
  }
}
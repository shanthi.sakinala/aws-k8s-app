<?php

$dsn = "mysql:host=".$_ENV[DB_HOST]."; dbname=".$_ENV[DB_NAME]."";
$user = $_ENV[DB_USER]; 
$password = $_ENV[DB_PASSWORD];
try
{
	$DB_con = new PDO($dsn, $user, $password);
	$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
	echo $e->getMessage();
}
include_once 'crud.php';
$crud = new crud($DB_con);
?>

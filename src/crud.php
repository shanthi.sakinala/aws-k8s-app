<?php
class crud
{
	private $db;
	function __construct($DB_con)
	{
		$this->db = $DB_con;
	}
	//Create user
	public function create($fname,$lname,$email,$phone)
	{
		try
		{
			$stmt = $this->db->prepare(
				"INSERT INTO tbl_users(first_name,last_name,email_id,phone)
						VALUES(:fname, :lname, :email, :phone)");
			$stmt->bindparam(":fname",$fname);
			$stmt->bindparam(":lname",$lname);
			$stmt->bindparam(":email",$email);
			$stmt->bindparam(":phone",$phone);
			return $stmt->execute();
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}	
	}
	
	public function getID($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM tbl_users WHERE id=:id");
		$stmt->execute(array(":id"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}
    //Update user
	public function update($id,$fname,$lname,$email,$phone)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE tbl_users SET first_name=:fname, 
		                                               last_name=:lname, 
													   email_id=:email, 
													   phone=:phone
													WHERE id=:id ");
			$stmt->bindparam(":fname",$fname);
			$stmt->bindparam(":lname",$lname);
			$stmt->bindparam(":email",$email);
			$stmt->bindparam(":phone",$phone);
			$stmt->bindparam(":id",$id);
			// execution de la requete :
			$stmt->execute();
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	public function delete($id)
	{
		$stmt = $this->db->prepare("DELETE FROM tbl_users WHERE id=:id");
		$stmt->bindparam(":id",$id);
		$stmt->execute();
		return true;
	}

	public function dataview($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		if($stmt->rowCount() > 0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				?>
                <tr>
                    <th><?php print($row['id']); ?></th>
                    <td><?php print($row['first_name']); ?></td>
                    <td><?php print($row['last_name']); ?></td>
                    <td><?php print($row['email_id']); ?></td>
                    <td><?php print($row['phone']); ?></td>
                    <td align="center">
                    <a href="edit-data.php?edit_id=<?php print($row['id']); ?>">
                    <i class="fas fa-user-edit"></i>
                    </a>
                    </td>
                    <td align="center">
                    <a href="delete.php?delete_id=<?php print($row['id']); ?>">
                    <i class="fas fa-user-slash" style='color:red'></i>
                    </a>
                    </td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
                <td>Aucun utilisateur...</td>
            </tr>
            <?php
		}
	}	
}
?>
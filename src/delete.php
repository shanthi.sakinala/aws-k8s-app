<?php
    include_once 'dbconfig.php';
    if(isset($_POST['btn-del']))
    {
        $id = $_GET['delete_id'];
        $crud->delete($id);
        header("Location: delete.php?deleted");
    }
?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SAMPLE CRUD PHP-MYSQL</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <?php
    include_once 'header.php';
    ?>
    <br/>
    <div class="container">
        <?php
        if(isset($_GET['deleted']))
        {
            ?>
            <div class="alert alert-success">
            suppression avec succés
            </div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-danger">
            sûre de faire la suppression
            </div>
            <?php
        }
        ?>
    </div>

    <div class="container">
    <?php
    if(isset($_GET['delete_id']))
    {
    ?>
        <table class='table'>
            <thead class="thead-light">
                <tr>
                    <th>N°</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Email</th>
                    <th>Tél</th>
                </tr>
            </thead
            <tbody>
                <?php
                $stmt = $DB_con->prepare("SELECT * FROM tbl_users WHERE id=:id");
                $stmt->execute(array(":id"=>$_GET['delete_id']));
                while($row=$stmt->fetch(PDO::FETCH_BOTH))
                {
                ?>
                <tr>
                    <td><?php print($row['id']); ?></td>
                    <td><?php print($row['first_name']); ?></td>
                    <td><?php print($row['last_name']); ?></td>
                    <td><?php print($row['email_id']); ?></td>
                    <td><?php print($row['phone']); ?></td>
                </tr>
                <?php
                }
             ?>
            </tbody>
        </table>
    <?php
    }
    ?>
    </div>

    <div class="container">
        <p>
        <?php
        if(isset($_GET['delete_id']))
        {
            ?>
            <form method="post">
                <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                <button class="btn btn-large btn-primary" type="submit" name="btn-del"><i class="fas fa-user-slash"></i> &nbsp; Oui</button>
                <a href="index.php" class="btn btn-large btn-success"><i class="fas fa-backward"></i> &nbsp; Non</a>
            </form>
            <?php
        }
        else
        {
            ?>
            <a href="index.php" class="btn btn-large btn-success" style="float: right;"><i class="fas fa-backward"></i> &nbsp; List des utilisateurs</a>
            <?php
        }
        ?>
        </p>
    </div>
    <?php include_once 'footer.php'; ?>
</body>
</html>
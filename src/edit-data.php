<?php
    include_once 'dbconfig.php';
    if(isset($_POST['btn-update']))
    {
        $id = $_GET['edit_id'];
        $fname = $_POST['first_name'];
        $lname = $_POST['last_name'];
        $email = $_POST['email_id'];
        $phone = $_POST['phone'];

        if($crud->update($id,$fname,$lname,$email,$phone))
        {
             header("Location: index.php");
        }
        else
        {
            $msg = "<div class='alert alert-warning'>
                    Erreur de modification
                    </div>";
        }
    }

    if(isset($_GET['edit_id']))
    {
        $id = $_GET['edit_id'];
        extract($crud->getID($id));
    }
?>

<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SAMPLE CRUD PHP-MYSQL</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>


    <?php include_once 'header.php'; ?>
    <div class="container">
    <?php
    if(isset($msg))
    {
        echo $msg;
    }
    ?>
    </div>
    <br/>
    <div class="container">
        <form method='post'>
            <table class='table'>
                <tr>
                    <td>Nom</td>
                    <td><input type='text' name='first_name' class='form-control' value="<?php echo $first_name; ?>" required></td>
                </tr>

                <tr>
                    <td>Prénom</td>
                    <td><input type='text' name='last_name' class='form-control' value="<?php echo $last_name; ?>" required></td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td><input type='text' name='email_id' class='form-control' value="<?php echo $email_id; ?>" required></td>
                </tr>

                <tr>
                    <td>Tél</td>
                    <td><input type='text' name='phone' class='form-control' value="<?php echo $phone; ?>" required></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <button type="submit" class="btn btn-primary" name="btn-update">
                            <span class="fas fa-user-edit"></span> Modifier l'utilisateur
                        </button>
                        <a href="index.php" class="btn btn-large btn-success" style="float: right;"><i class="fas fa-backward"></i> &nbsp; Annuler</a>
                    </td>
                </tr>
            </table>
            </form>
    </div>
    <?php include_once 'footer.php'; ?>
</body>
</html>
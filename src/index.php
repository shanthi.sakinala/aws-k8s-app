<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SAMPLE CRUD PHP-MYSQL</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <?php include_once 'dbconfig.php'; ?>
    <?php include_once 'header.php'; ?>
    <br/>
    <div class="container">
        <div>
            <a href="add-data.php" class="btn btn-large btn-info">
                <i class="fas fa-user-plus"></i> &nbsp; Ajouter un utilisateur
            </a>
        </div>
        <br />
        <div>
            <!--creation du tableau-->
            <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Id</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Email</th>
                        <th>Tél</th>
                        <th colspan="2" align="center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      $crud->dataview("SELECT * FROM tbl_users");
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php include_once 'footer.php'; ?>
</body>
</html>
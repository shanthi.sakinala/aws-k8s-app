<?php
    include_once 'dbconfig.php';
    if(isset($_POST['btn-save'])){
        $fname = $_POST['first_name'];
        $lname = $_POST['last_name'];
        $email = $_POST['email_id'];
        $phone = $_POST['phone'];
        if($crud->create($fname,$lname,$email,$phone)){
            header("Location: add-data.php?inserted");
        }else{
            header("Location: add-data.php?failure");
        }
    }
?>
<html>
<!DOCTYPE head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SAMPLE CRUD PHP-MYSQL</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <?php include_once 'header.php';
    if(isset($_GET['inserted'])){
        ?>
        <div class="container">
           <div class="alert alert-info">
            Insertion avec success
           </div>
        </div>
        <?php
    }else if(isset($_GET['failure'])){
        ?>
        <div class="container">
           <div class="alert alert-warning">
            Erreur d'insertion
           </div>
        </div>
        <?php
        }
    ?>
    <br/>
    <div class="container">
        <form method="post">
            <table class="table">
                <tr>
                    <td>Nome</td><td><input type='text' name='first_name' class='form-control' required></td>
                </tr>
                <tr>
                    <td>Prénom</td><td><input type='text' name='last_name' class='form-control' required></td>
                </tr>
                <tr>
                    <td>E - mail</td><td><input type='text' name='email_id' class='form-control' required></td>
                </tr>
                <tr>
                    <td>Tél</td><td><input type='text' name='phone' class='form-control' required></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="submit" class="btn btn-primary" name="btn-save">
                            <span class="fas fa-user-plus"></span> Crée l'utilisateur
                        </button>
                        <a href="index.php" class="btn btn-large btn-success" style="float: right;">
                            <i class="fas fa-backward"></i> &nbsp; List des utilisateurs
                        </a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <?php include_once 'footer.php'; ?>
</body>
</html>
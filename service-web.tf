resource "kubernetes_service" "svc_web" {
  metadata {
    name = "svc-web"
  }
  spec {
    selector = {
      app = var.web_lbl
    }
    port {
      port = var.web_port
    }
    cluster_ip = "None"
  }
}

resource "kubernetes_persistent_volume_claim" "pvc_mysql" {
  metadata {
    name = "pvc-mysql"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    volume_name = element(kubernetes_persistent_volume.pv_mysql.metadata.*.name, 0)
  }
}

resource "kubernetes_deployment" "dep-web" {
  metadata {
    name = "deploy-php-web"
    labels = {
      app = var.web_lbl
    }
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = var.web_lbl
      }
    }
    template {
      metadata {
        labels = {
          app = var.web_lbl
        }
      }
      spec {
        container {
          image = var.web_img
          name  = var.web_lbl
          //image_pull_policy = "IfNotPresent"
          env {
            name = "DB_HOST"
            value = element(kubernetes_service.svc-mysql.metadata.*.name, 0)
          }
          env {
            name = "DB_NAME"
            value = var.db_name
          }
          env {
            name = "DB_USER"
            value = "root"
          }
          env {
            name = "DB_PASSWORD"
            value_from {
              secret_key_ref {
                name = element(kubernetes_secret.mysql.metadata.*.name, 0)
                key = "db-password"
              }
            }
          }
          port {
            container_port = var.web_port
            name = var.web_lbl
          }
        }
      }
    }
  }
}